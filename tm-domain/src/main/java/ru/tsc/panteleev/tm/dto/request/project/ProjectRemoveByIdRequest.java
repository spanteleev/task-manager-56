package ru.tsc.panteleev.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectRemoveByIdRequest(@Nullable String token, @Nullable String id) {
        super(token);
        this.id = id;
    }

}
