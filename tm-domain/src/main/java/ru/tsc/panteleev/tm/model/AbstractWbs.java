package ru.tsc.panteleev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractWbs extends AbstractUserOwnedModel {

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @NotNull
    @Column(name = "created_dt")
    protected Date created = new Date();

    @Nullable
    @Column(name = "begin_dt")
    protected Date dateBegin;

    @Nullable
    @Column(name = "end_dt")
    protected Date dateEnd;

    @Override
    public String toString() {
        return "ID : " + id +
                " || NAME : " + name +
                " || DESCRIPTION : " + description +
                " || STATUS : " + status.getDisplayName() +
                " || CREATED : " + created +
                " || BEGIN : " + dateBegin +
                " || END : " + dateEnd;
    }

}
