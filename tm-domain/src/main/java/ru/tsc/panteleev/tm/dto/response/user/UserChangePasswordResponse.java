package ru.tsc.panteleev.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractResultResponse {

    public UserChangePasswordResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
