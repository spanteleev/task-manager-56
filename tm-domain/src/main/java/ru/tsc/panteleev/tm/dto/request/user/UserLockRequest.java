package ru.tsc.panteleev.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Setter
@Getter
@NoArgsConstructor
public class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(@Nullable String token, @Nullable String login) {
        super(token);
        this.login = login;
    }

}
