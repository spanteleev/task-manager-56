package ru.tsc.panteleev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import ru.tsc.panteleev.tm.listener.EntityListener;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO extends AbstractWbsDTO {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    protected String projectId = null;

}
