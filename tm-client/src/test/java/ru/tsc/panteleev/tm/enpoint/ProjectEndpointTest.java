package ru.tsc.panteleev.tm.enpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IUserEndpoint;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.dto.request.project.*;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserRegistryRequest;
import ru.tsc.panteleev.tm.dto.response.project.ProjectCreateResponse;
import ru.tsc.panteleev.tm.dto.response.project.ProjectListResponse;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.marker.SoapCategory;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.service.PropertyService;

import java.util.UUID;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Before
    public void getToken() {
        userToken = authEndpoint.login(new UserLoginRequest("test", "test")).getToken();
    }

    @After
    public void clear() {
        userToken = null;
    }

    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest("", "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(UUID.randomUUID().toString(), "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(userToken, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(userToken, null, "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(userToken, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(new ProjectCreateRequest(userToken, "", null, null, null)));
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable final ProjectDTO project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void listProject() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest("", null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.listProject(new ProjectListRequest(UUID.randomUUID().toString(), null)));
        @Nullable final ProjectListResponse response =
                projectEndpoint.listProject(new ProjectListRequest(userToken, null));
        Assert.assertNotNull(response.getProjects());
    }

    @Test
    public void clearProject() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        userEndpoint.registrationUser(new UserRegistryRequest(login, password, email));
        @NotNull final String token = authEndpoint.login(new UserLoginRequest(login, password)).getToken();
        projectEndpoint.createProject(new ProjectCreateRequest(token, UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null));
        projectEndpoint.createProject(new ProjectCreateRequest(token, UUID.randomUUID().toString(), UUID.randomUUID().toString(), null, null));
        Assert.assertEquals(2,
                projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects().size());
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.clearProject(new ProjectClearRequest()));
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
    }

    @Test
    public void showProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(Exception.class, () -> projectEndpoint.showProjectById(new ProjectGetByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectGetByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectGetByIdRequest(userToken, "")));
        project = projectEndpoint.showProjectById(new ProjectGetByIdRequest(userToken, projectId)).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(name, project.getName());
        Assert.assertEquals(description, project.getDescription());
    }

    @Test
    public void updateProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        @NotNull final String newName = UUID.randomUUID().toString();
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest("", "", "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, "", "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, null, "", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, projectId, null, "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, null, newName, "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, projectId, newName, null)));
        project = projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(userToken, projectId, newName, newDescription)).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test
    public void removeProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(userToken, "")));
        @NotNull final String removeProjectId =
                projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(userToken, projectId)).getProject().getId();
        Assert.assertEquals(projectId, removeProjectId);
    }

    @Test
    public void changeProjectStatus() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest("", "", null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(userToken, "", null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(userToken, projectId, null)));
        project = projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(userToken, projectId, Status.IN_PROGRESS)).getProject();
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        project = projectEndpoint.changeProjectStatusById(new ProjectChangeStatusByIdRequest(userToken, projectId, Status.COMPLETED)).getProject();
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

    @Test
    public void startProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.startProjectById(new ProjectStartByIdRequest(userToken, "")));
        project = projectEndpoint.startProjectById(new ProjectStartByIdRequest(userToken, projectId)).getProject();
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void completeProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @Nullable final ProjectCreateResponse response =
                projectEndpoint.createProject(new ProjectCreateRequest(userToken, name, description, null, null));
        @Nullable ProjectDTO project = response.getProject();
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest()));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest("", "")));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(userToken, "")));
        project = projectEndpoint.completeProjectStatusById(new ProjectCompleteByIdRequest(userToken, projectId)).getProject();
        Assert.assertEquals(Status.COMPLETED, project.getStatus());
    }

}
