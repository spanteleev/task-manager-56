package ru.tsc.panteleev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.ICommandRepository;
import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Repository
public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> mapByName = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> mapByArgument = new TreeMap<>();

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        @NotNull final String name = command.getName();
        if (!name.isEmpty())
            mapByName.put(name, command);
        @Nullable final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty())
            mapByArgument.put(argument, command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return mapByName.values();
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(final String name) {
        return mapByName.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        return mapByArgument.get(argument);
    }

    @NotNull
    @Override
    public Iterable<AbstractCommand> getCommandsWithArgument() {
        return mapByArgument.values();
    }

}
