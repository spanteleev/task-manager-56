package ru.tsc.panteleev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.project.ProjectCreateRequest;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Date;

@Component
public class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @Nullable final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END:");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        getProjectEndpoint().createProject(new ProjectCreateRequest(getToken(), name, description, dateBegin, dateEnd));
    }

}
