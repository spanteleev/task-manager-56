package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskGetListByProjectIdRequest;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskShowListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list-show-by-project-id";

    @NotNull
    public static final String DESCRIPTION = "Show task list by project id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final List<TaskDTO> tasks = getTaskEndpoint().showListByProjectIdTask(
                new TaskGetListByProjectIdRequest(getToken(), projectId)).getTasks();
        renderTasks(tasks);
    }

}
