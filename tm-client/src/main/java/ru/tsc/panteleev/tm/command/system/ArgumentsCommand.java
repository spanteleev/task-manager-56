package ru.tsc.panteleev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

@Component
public class ArgumentsCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.stream().filter(command -> command.getArgument() != null && !command.getArgument().isEmpty())
                .forEach(command -> System.out.println(command.getArgument()));
    }

}
