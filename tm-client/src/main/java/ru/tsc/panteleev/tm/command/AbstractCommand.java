package ru.tsc.panteleev.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.service.ITokenService;
import ru.tsc.panteleev.tm.enumerated.Role;

@Getter
@Setter
@Component
public abstract class AbstractCommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public abstract void execute();

    public abstract Role[] getRoles();

    protected String getToken() {
        return getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getTokenService().setToken(token);
    }

    @Override
    public String toString() {
        String result = "";
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        if (name != null && !name.isEmpty()) result += "Command '" + name + "'";
        if (argument != null && !argument.isEmpty()) result += ", argument '" + argument + "'";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
