package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataBase64LoadRequest;

@Component
public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file";

    @NotNull
    private static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

}
