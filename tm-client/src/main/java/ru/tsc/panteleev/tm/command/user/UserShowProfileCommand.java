package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.user.UserProfileRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

@Component
public class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-show-profile";

    @NotNull
    public static final String DESCRIPTION = "Show user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW PROFILE]");
        @Nullable final UserDTO user = getAuthEndpoint().profile(new UserProfileRequest(getToken())).getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
