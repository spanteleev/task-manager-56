package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.user.UserLockRequest;
import ru.tsc.panteleev.tm.dto.response.user.UserLockResponse;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class UserLockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-lock";

    @NotNull
    public static final String DESCRIPTION = "Lock user.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserLockResponse response = getUserEndpoint().lockUser(new UserLockRequest(getToken(), login));
        if (!response.getSuccess())
            System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
