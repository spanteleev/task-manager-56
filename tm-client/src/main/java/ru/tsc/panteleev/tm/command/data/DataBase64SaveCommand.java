package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataBase64SaveRequest;

@Component
public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in base64 file";

    @NotNull
    private static final String NAME = "data-save-base64";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

}
