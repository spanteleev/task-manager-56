package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataBackupLoadRequest;

@Component
public class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load backup";

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();

        getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest(getToken()));
    }

}
