package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataYamlFasterXmlLoadRequest;

@Component
public class DataYamlFasterXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    public static final String NAME = "data-load-yaml-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().loadDataYamlFasterXml(new DataYamlFasterXmlLoadRequest(getToken()));
    }

}
