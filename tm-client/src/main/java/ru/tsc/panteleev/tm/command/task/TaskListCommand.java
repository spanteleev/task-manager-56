package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.task.TaskListRequest;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT TYPE:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final List<TaskDTO> tasks = getTaskEndpoint().listTask(
                new TaskListRequest(getToken(), sort)).getTasks();
        renderTasks(tasks);
    }

}
