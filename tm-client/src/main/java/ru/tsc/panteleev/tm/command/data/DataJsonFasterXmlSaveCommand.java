package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.data.DataJsonFasterXmlSaveRequest;

import java.io.FileOutputStream;

@Component
public class DataJsonFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Save data in json file";

    @NotNull
    private static final String NAME = "data-save-json-fasterxml";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest(getToken()));
    }

}
