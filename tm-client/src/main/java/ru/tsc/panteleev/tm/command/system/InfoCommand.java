package ru.tsc.panteleev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.util.NumberUtil;

@Component
public class InfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String DESCRIPTION = "Show system info.";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[SYSTEM INFO]");
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (threads): " + availableProcessors);
        final long maxMemory = runtime.maxMemory();
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryFormat = isMemoryLimit ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.println("Maximum memory available: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available: " + NumberUtil.formatBytes(totalMemory));
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

}
