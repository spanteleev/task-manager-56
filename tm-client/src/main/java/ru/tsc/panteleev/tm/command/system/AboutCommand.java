package ru.tsc.panteleev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.response.system.ServerAboutResponse;

@Component
public class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        ServerAboutResponse response = getSystemEndpoint().getAbout(new ServerAboutRequest());
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
