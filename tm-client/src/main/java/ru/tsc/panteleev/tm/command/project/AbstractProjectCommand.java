package ru.tsc.panteleev.tm.command.project;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;
import ru.tsc.panteleev.tm.util.DateUtil;

import java.util.List;

@Getter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    protected IProjectTaskEndpoint projectTaskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderProjects(@NotNull final List<ProjectDTO> projects) {
        for (final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(project);
        }
    }

    protected void showProject(@NotNull final ProjectDTO project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
