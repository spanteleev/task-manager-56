package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.response.user.UserLoginResponse;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.util.TerminalUtil;

@Component
public class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-login";

    @NotNull
    public static final String DESCRIPTION = "Log in.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginResponse response = getAuthEndpoint().login(new UserLoginRequest(login, password));
        setToken(response.getToken());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
