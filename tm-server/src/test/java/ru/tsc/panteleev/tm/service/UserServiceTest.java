package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.panteleev.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.panteleev.tm.configuration.ServerConfiguration;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;

import java.util.UUID;

public class UserServiceTest {

    @NotNull
    private IUserServiceDTO userService;

    @NotNull
    private final String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @Nullable
    private final static String STRING_NULL = null;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserServiceDTO.class);
    }

    @Test
    public void create() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginExistsException.class, () -> userService.create(login, password, email));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(STRING_NULL, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(STRING_RANDOM, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(STRING_RANDOM, STRING_NULL, STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(STRING_RANDOM, STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.create(STRING_RANDOM, STRING_RANDOM, STRING_NULL));
        Assert.assertThrows(EmailExistsException.class, () -> userService.create(STRING_RANDOM, STRING_RANDOM, email));
    }

    @Test
    public void findByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(STRING_NULL));
        Assert.assertNotNull(userService.findByLogin(login));
    }

    @Test
    public void findByEmail() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(STRING_EMPTY));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(STRING_NULL));
        Assert.assertNotNull(userService.findByEmail(email));
    }

    @Test
    public void isLoginExists() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(userService.isLoginExists(login));
        Assert.assertFalse(userService.isLoginExists(STRING_EMPTY));
        Assert.assertFalse(userService.isLoginExists(STRING_NULL));
        Assert.assertFalse(userService.isLoginExists(STRING_RANDOM));
    }

    @Test
    public void isEmailExists() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(userService.isEmailExists(email));
        Assert.assertFalse(userService.isEmailExists(STRING_EMPTY));
        Assert.assertFalse(userService.isEmailExists(STRING_NULL));
        Assert.assertFalse(userService.isEmailExists(STRING_RANDOM));
    }

    @Test
    public void removeByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertTrue(userService.isLoginExists(login));
        userService.removeByLogin(login);
        Assert.assertFalse(userService.isLoginExists(login));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(STRING_NULL));
    }

    @Test
    public void setPassword() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(STRING_NULL, STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), STRING_EMPTY));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), STRING_NULL));
        Assert.assertNotNull(userService.setPassword(user.getId(), STRING_RANDOM));
    }

    @Test
    public void updateUser() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(STRING_EMPTY, STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertThrows(IdEmptyException.class, () -> userService.updateUser(STRING_NULL, STRING_EMPTY, STRING_EMPTY, STRING_EMPTY));
        Assert.assertNotNull(userService.updateUser(user.getId(), STRING_RANDOM, STRING_RANDOM, STRING_RANDOM));
        @NotNull final UserDTO userAfterUpdate = userService.findByLogin(login);
        Assert.assertNotNull(userAfterUpdate.getFirstName());
        Assert.assertNotNull(userAfterUpdate.getLastName());
        Assert.assertNotNull(userAfterUpdate.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(STRING_NULL));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.lockUserByLogin(STRING_RANDOM));
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(login);
        @NotNull final UserDTO userAfterUpdate = userService.findByLogin(login);
        Assert.assertTrue(userAfterUpdate.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        @NotNull final String login = UUID.randomUUID().toString();
        @NotNull final String password = UUID.randomUUID().toString();
        @NotNull final String email = UUID.randomUUID().toString();
        @Nullable final UserDTO user = userService.create(login, password, email);
        Assert.assertNotNull(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(STRING_EMPTY));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(STRING_NULL));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.unlockUserByLogin(STRING_RANDOM));
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(login);
        @NotNull UserDTO userAfterUpdate = userService.findByLogin(login);
        Assert.assertTrue(userAfterUpdate.getLocked());
        userService.unlockUserByLogin(login);
        userAfterUpdate = userService.findByLogin(login);
        Assert.assertFalse(userAfterUpdate.getLocked());
    }

}
