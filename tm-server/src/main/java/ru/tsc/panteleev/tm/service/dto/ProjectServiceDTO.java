package ru.tsc.panteleev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO> implements IProjectServiceDTO {

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository() {
        return context.getBean(IProjectRepositoryDTO.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(@Nullable final String userId,
                             @Nullable final String name,
                             @Nullable final String description,
                             @Nullable final Date dateBegin,
                             @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project.setUserId(userId);
            project.setName(name);
            project.setDescription(description);
            project.setDateBegin(dateBegin);
            project.setDateEnd(dateEnd);
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO project = findById(userId, id);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project.setName(name);
            project.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeStatusById(@Nullable final String userId,
                                       @Nullable String id,
                                       @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @NotNull final ProjectDTO project = findById(userId, id);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (sort == null) return findAll(userId);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUserIdSort(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            ProjectDTO project = repository.findById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO project;
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            project = repository.findById(userId, id);
            if (project == null) return;
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@NotNull String userId) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull Collection<ProjectDTO> projects) {
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.set(projects);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
