package ru.tsc.panteleev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;

import java.util.Date;
import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(String userId, String projectId);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description,
                   @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name,
                       @Nullable String description);

    @NotNull
    TaskDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    void update(@NotNull TaskDTO task);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
