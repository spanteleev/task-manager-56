package ru.tsc.panteleev.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class UserRepositoryDTO extends AbstractRepositoryDTO<UserDTO> implements IUserRepositoryDTO {

    @Override
    public void set(@NotNull Collection<UserDTO> users) {
        clear();
        for (UserDTO user : users)
            add(user);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    public void removeById(@NotNull String id) {
        remove(findById(id));
    }

    @Override
    public void clear() {
        @NotNull final List<UserDTO> users = findAll();
        for (UserDTO user : users)
            remove(user);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDTO u", Long.class).getSingleResult();
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull String email) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

}
