package ru.tsc.panteleev.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected String getSortColumn(@Nullable final Sort sort) {
        return sort.getOrderColumn();
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
