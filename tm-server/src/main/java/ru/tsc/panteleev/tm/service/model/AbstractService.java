package ru.tsc.panteleev.tm.service.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.model.IRepository;
import ru.tsc.panteleev.tm.api.service.model.IService;
import ru.tsc.panteleev.tm.model.AbstractModel;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @Autowired
    protected ApplicationContext context;

    protected abstract IRepository<M> getRepository();

}
