package ru.tsc.panteleev.tm.service.dto;

import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

@Service
public abstract class AbstractUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {

    protected abstract IUserOwnedRepositoryDTO<M> getRepository();

}
