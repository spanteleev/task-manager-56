package ru.tsc.panteleev.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Getter
@Repository
@Scope("prototype")
public class TaskRepositoryDTO extends AbstractUserOwnedRepositoryDTO<TaskDTO> implements ITaskRepositoryDTO {

    @Override
    public void set(@NotNull Collection<TaskDTO> tasks) {
        clear();
        for (TaskDTO task : tasks)
            add(task);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserId(@NotNull String userId) {
        return entityManager.createQuery("FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort) {
        if (sort == null) return findAllByUserId(userId);
        @NotNull final String query =
                String.format("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.%s", getSortColumn(sort));
        return entityManager.createQuery(query, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return entityManager.createQuery("FROM TaskDTO", TaskDTO.class).getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull String userId, @NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultStream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        remove(findById(userId, id));
    }

    @Override
    public void clearByUserId(@NotNull String userId) {
        @NotNull final List<TaskDTO> tasks = findAllByUserId(userId);
        for (TaskDTO task : tasks)
            remove(task);
    }

    @Override
    public void clear() {
        @NotNull final List<TaskDTO> tasks = findAll();
        for (TaskDTO task : tasks)
            remove(task);
    }

    @Override
    public long getSize(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t", Long.class).getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        return findById(userId, id) != null;
    }

    @Override
    public @NotNull List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) {
        return entityManager
                .createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final List<TaskDTO> tasks = findAllByProjectId(userId, projectId);
        for (TaskDTO task : tasks)
            remove(task);
    }


}
