package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    void set(@NotNull Collection<TaskDTO> tasks);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByUserIdSort(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    List<TaskDTO> findAll();

    @Nullable
    TaskDTO findById(@NotNull String userId, @NotNull String id);

    void removeById(@NotNull String userId, @NotNull String id);

    void clearByUserId(@NotNull String userId);

    void clear();

    long getSize(@NotNull String userId);

    boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
