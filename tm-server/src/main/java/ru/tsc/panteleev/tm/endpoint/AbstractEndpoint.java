package ru.tsc.panteleev.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;
import ru.tsc.panteleev.tm.dto.model.SessionDTO;

@Getter
@Controller
@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    protected SessionDTO check(AbstractUserRequest request, Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @Nullable final SessionDTO session = getAuthService().validateToken(token);
        @Nullable final Role roleUser = session.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected SessionDTO check(AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getAuthService().validateToken(token);
    }

}
