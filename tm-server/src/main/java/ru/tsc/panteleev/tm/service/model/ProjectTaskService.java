package ru.tsc.panteleev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.model.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.model.ITaskRepository;
import ru.tsc.panteleev.tm.api.service.model.IProjectService;
import ru.tsc.panteleev.tm.api.service.model.IProjectTaskService;
import ru.tsc.panteleev.tm.api.service.model.ITaskService;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.UserIdEmptyException;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @SneakyThrows
    public void bindTaskToProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @Nullable final Task task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProject(project);
        taskService.update(task);
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Task task = Optional.ofNullable(taskService.findById(userId, taskId))
                .orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        taskService.update(task);
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        taskService.removeTasksByProjectId(userId, projectId);
        projectService.removeById(userId, projectId);
    }

}
