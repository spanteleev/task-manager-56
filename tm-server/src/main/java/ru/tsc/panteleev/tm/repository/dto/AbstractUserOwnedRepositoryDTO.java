package ru.tsc.panteleev.tm.repository.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

@Getter
@Repository
@Scope("prototype")
public class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

}
