package ru.tsc.panteleev.tm.repository.model;

import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

@Getter
@Repository
@Scope("prototype")
public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

}
