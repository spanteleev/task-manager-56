package ru.tsc.panteleev.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.field.LoginEmptyException;
import ru.tsc.panteleev.tm.exception.field.PasswordEmptyException;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;
import ru.tsc.panteleev.tm.exception.user.LoginOrPasswordIncorrectException;
import ru.tsc.panteleev.tm.dto.model.SessionDTO;
import ru.tsc.panteleev.tm.dto.model.UserDTO;
import ru.tsc.panteleev.tm.util.CryptUtil;
import ru.tsc.panteleev.tm.util.HashUtil;

import java.util.Date;

@Service
public class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @NotNull
    @Autowired
    private IUserServiceDTO userService;

    @NotNull
    @Override
    public String login(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (user.getLocked()) throw new LoginOrPasswordIncorrectException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new AccessDeniedException();
        if (!passwordHash.equals(user.getPasswordHash())) throw new LoginOrPasswordIncorrectException();
        return getToken(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO validateToken(@Nullable String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @Nullable String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull SessionDTO session = objectMapper.readValue(json, SessionDTO.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = ((currentDate.getTime() - sessionDate.getTime()) / 1000);
        if (delta > propertyService.getSessionTimeout()) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDTO user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDTO session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    private SessionDTO createSession(UserDTO user) {
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        return session;
    }


    @Override
    public UserDTO registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

}
