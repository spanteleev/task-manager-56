package ru.tsc.panteleev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.ITaskRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.dto.ITaskServiceDTO;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.*;
import ru.tsc.panteleev.tm.dto.model.TaskDTO;
import ru.tsc.panteleev.tm.repository.dto.TaskRepositoryDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public class TaskServiceDTO extends AbstractUserOwnedServiceDTO<TaskDTO, ITaskRepositoryDTO> implements ITaskServiceDTO {

    @NotNull
    protected ITaskRepositoryDTO getRepository() {
        return context.getBean(TaskRepositoryDTO.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO create(@Nullable final String userId,
                          @Nullable final String name,
                          @Nullable final String description,
                          @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setUserId(userId);
            task.setName(name);
            task.setDescription(description);
            task.setDateBegin(dateBegin);
            task.setDateEnd(dateEnd);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = findById(userId, id);
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setName(name);
            task.setDescription(description);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskDTO changeStatusById(@Nullable final String userId,
                                    @Nullable String id,
                                    @Nullable Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @NotNull final TaskDTO task = findById(userId, id);
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task.setStatus(status);
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull String userId) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUserId(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull String userId, @Nullable Sort sort) {
        if (sort == null) return findAll(userId);
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByUserIdSort(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDTO findById(@NotNull String userId, @NotNull String id) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            TaskDTO task = repository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            task = repository.findById(userId, id);
            if (task == null) return;
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clearByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@NotNull String userId) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getSize(userId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.existsById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull Collection<TaskDTO> tasks) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.set(tasks);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public void update(@NotNull TaskDTO task) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId) {
        @NotNull final ITaskRepositoryDTO repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.removeTasksByProjectId(userId,projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
